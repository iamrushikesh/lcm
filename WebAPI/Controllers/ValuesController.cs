﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public object Post([FromBody] int[] value)
        {
            int CalculateGcd(int a, int b)
            {
                while (true)
                {
                    if (a == 0 || b == 0) return 0;

                    if (a == b) return a;

                    if (a > b)
                    {
                        a = a - b;
                        continue;
                    }

                    var a1 = a;
                    b = b - a1;
                }
            }

            int CalculateLcm(int a, int b) 
            { 
                return (a * b) / CalculateGcd(a, b); 
            } 
            
            var lcm = value[0]; 
                for (var i = 1; i < value.Length; i++) 
                    lcm = CalculateLcm(value[i], lcm);

            return lcm; 
             
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
