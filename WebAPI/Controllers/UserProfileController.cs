﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AuthenticationContext _context;

        public UserProfileController(UserManager<ApplicationUser> userManager, AuthenticationContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        [HttpGet]
        [Authorize]
        //GET : /api/UserProfile
        public async Task<object> GetUserProfile()
        {
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            var user = await _userManager.FindByIdAsync(userId);

            return new
            {
                user.Id,
                user.FullName,
                user.Email,
                user.UserName
            };
        }

        [HttpGet]
        [Route("LcmHistory")]
        //GET : /api/UserProfile/LcmHistory
        public IQueryable<LcmHistory> Get()
        {
            var userId = User.Claims.FirstOrDefault(c => c.Type == "UserID")?.Value;
            var history = _context.LcmHistories.Where(e => e.UserId == userId);
            return history;
        }

        [HttpPost]
        [Route("Lcm")]
        //POST : /api/UserProfile/Lcm
        public async Task<long> Post([FromBody] int[] numbers)
        {
            var nums = string.Join(",", numbers);

            int CalculateGcd(int a, int b)
            {
                while (true)
                {
                    if (a == 0 || b == 0) return 0;

                    if (a == b) return a;

                    if (a > b)
                    {
                        a = a - b;
                        continue;
                    }

                    var a1 = a;
                    b = b - a1;
                }
            }

            int CalculateLcm(int a, int b)
            {
                return (a * b) / CalculateGcd(a, b);
            }

            var lcm = numbers[0];
            for (var i = 1; i < numbers.Length; i++)
                lcm = CalculateLcm(numbers[i], lcm);

            var userId = User.Claims.First(c => c.Type == "UserID").Value;

            var lcmHistory = new LcmHistory {UserId = userId, Input = nums, Result = lcm.ToString()};
            await _context.LcmHistories.AddAsync(lcmHistory);
            await _context.SaveChangesAsync();
            return lcm;
        }

        [HttpPost]
        [Route("LcmFact")]
        //POST : /api/UserProfile/LcmFact
        public async Task<long> LcmFact([FromBody] int[] numbers)
        {
            var nums = string.Join(",", numbers);
            long lcm = 1;
            var divisor = 2;
            while (true)
            {
                var cnt = 0;
                var divisible = false;
                for (var i = 0; i < numbers.Length; i++)
                {
                    if (numbers[i] == 0)
                    {
                        return 0;
                    }

                    if (numbers[i] < 0)
                    {
                        numbers[i] = numbers[i] * (-1);
                    }

                    if (numbers[i] == 1)
                    {
                        cnt++;
                    }

                    if (numbers[i] % divisor != 0) continue;
                    divisible = true;
                    numbers[i] = numbers[i] / divisor;
                }

                if (divisible)
                {
                    lcm = lcm * divisor;
                }
                else
                {
                    divisor++;
                }

                if (cnt == numbers.Length)
                {
                    var userId = User.Claims.First(c => c.Type == "UserID").Value;

                    var lcmHistory = new LcmHistory {UserId = userId, Input = nums, Result = lcm.ToString()};
                    await _context.LcmHistories.AddAsync(lcmHistory);
                    await _context.SaveChangesAsync();
                    return lcm;
                }
            }
        }
    }
}